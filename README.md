#Install SQLite on Linux

Download SQLite

To download SQLite, you open the [download page](https://www.sqlite.org/download.html) of the SQlite website.

Today, almost all the flavours of Linux OS are being shipped with SQLite. So you just issue the following command to check if you already have SQLite installed on your machine.

$sqlite3
SQLite version 3.7.15.2 2013-01-09 11:53:05
Enter ".help" for instructions
Enter SQL statements terminated with a ";"
sqlite>
If you do not see the above result, then it means you do not have SQLite installed on your Linux machine. Following are the following steps to install SQLite −

Step 1 − Go to SQLite download page and download sqlite-autoconf-*.tar.gz from source code section.

Step 2 − Run the following command −

$tar xvfz sqlite-autoconf-3071502.tar.gz
$cd sqlite-autoconf-3071502
$./configure --prefix=/usr/local
$sudo make
$sudo make install
The above command will end with SQLite installation on your Linux machine. Which you can verify as explained above.

#Install Maven
$ sudo apt-get install maven
