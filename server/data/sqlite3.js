//https://www.reindex.io/blog/building-a-graphql-server-with-node-js-and-sql/

import sqlite3 from 'sqlite3';
import {promisify} from 'bluebird';

const db = new sqlite3.Database('db.sqlite3');

db.get = promisify(db.get);
db.all = promisify(db.all);

graphql(Schema, `{
  viewer {
    id,
    name
  }
}`, {db}).then((result) => console.log(result));
