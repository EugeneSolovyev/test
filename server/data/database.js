class User {
  constructor(id, name, username, website) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.website = website;
  }
}

class Driver {
  constructor(id, name, modelAuto, from, to, img) {
    this.id = id;
    this.name = name;
    this.modelAuto = modelAuto;
    this.from = from;
    this.to = to;
    this.img = img;
  }
}

const lvarayut = new User('1', 'Eugene Solovyev', 'EugeneSolovyev', 'https://github.com/EugeneSolovyev');

const drivers = [];
let driversCount = 0;
function addDriver(name, modelAuto, from, to, img) {
  const newDriver = new Driver(driversCount, name, modelAuto, from, to, img);
  drivers.push(newDriver);
  newDriver.id = driversCount;
  driversCount += 1;
  return newDriver;
}

function getDriver(id) {
  return drivers.find(driver => driver.id === id);
}

function getAllDrivers() {
  return drivers;
}

function getUser(id) {
  return id === lvarayut.id ? lvarayut : null;
}

export {
  User,
  Driver,
  getUser,
  addDriver,
  getDriver,
  getAllDrivers
};
