/* eslint-disable no-unused-vars, no-use-before-define */
import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  fromGlobalId,
  globalIdField,
  mutationWithClientMutationId,
  nodeDefinitions,
  cursorForObjectInConnection
} from 'graphql-relay';

import {
  User,
  Driver,
  getUser,
  addDriver,
  getDriver,
  getAllDrivers
} from './database';


/**
 * We get the node interface and field from the Relay library.
 *
 * The first method defines the way we resolve an ID to its object.
 * The second defines the way we resolve an object to its GraphQL type.
 */
const { nodeInterface, nodeField } = nodeDefinitions(
  (globalId) => {
    const { type, id } = fromGlobalId(globalId);
    if (type === 'User') {
      return getUser(id);
    } else if (type === 'Driver') {
      return getDriver(id);
    }
    return null;
  },
  (obj) => {
    if (obj instanceof User) {
      return userType;
    } else if (obj instanceof Driver) {
      return driverType;
    }
    return null;
  }
);

/**
 * Define your own types here
 */

const userType = new GraphQLObjectType({
  name: 'User',
  description: 'A person who uses our app',
  fields: () => ({
    id: globalIdField('User'),
    drivers: {
      type: driverConnection,
      description: 'Drivers that are available',
      args: connectionArgs,
      resolve: (_, args) => connectionFromArray(getAllDrivers(), args)
    },
    username: {
      type: GraphQLString,
      description: 'Users\'s username'
    },
    website: {
      type: GraphQLString,
      description: 'User\'s website'
    }
  }),
  interfaces: [nodeInterface]
});

const driverType = new GraphQLObjectType({
  name: 'Driver',
  description: 'Driver for works',
  fields: () => ({
    id: globalIdField('Driver'),
    name: {
      type: GraphQLString,
      description: 'Name of the driver'
    },
    modelAuto: {
      type: GraphQLString,
      description: 'model auto of the driver'
    },
    from: {
      type: GraphQLString,
      description: 'start day of available'
    },
    to: {
      type: GraphQLString,
      description: 'end day of available'
    },
    img: {
      type: GraphQLString,
      description: 'driver\'s auto'
    }
  }),
  interfaces: [nodeInterface]
});

const { connectionType: driverConnection, edgeType: driverEdge } = connectionDefinitions({ name: 'Driver', nodeType: driverType });

/**
 * Create feature example
 */

const addDriverMutation = mutationWithClientMutationId({
  name: 'AddDriver',
  inputFields: {
    name: { type: new GraphQLNonNull(GraphQLString) },
    modelAuto: { type: new GraphQLNonNull(GraphQLString) },
    from: { type: new GraphQLNonNull(GraphQLString) },
    to: { type: new GraphQLNonNull(GraphQLString) },
    img: { type: new GraphQLNonNull(GraphQLString) }
  },

  outputFields: {
    driverEdge: {
      type: driverEdge,
      resolve: (obj) => {
        const cursorId = cursorForObjectInConnection(getAllDrivers(), obj);
        return { node: obj, cursor: cursorId };
      }
    },
    viewer: {
      type: userType,
      resolve: () => getUser('1')
    }
  },

  mutateAndGetPayload: ({ name, modelAuto, from, to, img }) => addDriver(name, modelAuto, from, to, img)
});


/**
 * This is the type that will be the root of our query,
 * and the entry point into our schema.
 */
const queryType = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    node: nodeField,
    // Add your own root fields here
    viewer: {
      type: userType,
      resolve: () => getUser('1')
    }
  })
});

/**
 * This is the type that will be the root of our mutations,
 * and the entry point into performing writes in our schema.
 */
const mutationType = new GraphQLObjectType({
  name: 'Mutation',
  fields: () => ({
    addDriver: addDriverMutation
  })
});

/**
 * Finally, we construct our schema (whose starting query type is the query
 * type we defined above) and export it.
 */
export default new GraphQLSchema({
  query: queryType,
  mutation: mutationType
});
