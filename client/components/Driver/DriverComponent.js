import React from 'react';
import Relay from 'react-relay';
import { Grid, Cell, Button, Card, CardTitle, CardText, CardActions, Textfield } from 'react-mdl';
import Page from '../Page/PageComponent';
import AddDriverComponent from './AddDriverComponent';

export default class Driver extends React.Component {

  static propTypes = {
    viewer: React.PropTypes.object.isRequired
  };

  render() {
    return (
      <Page heading="Find Driver">
        <div style={{ width: '100%', margin: 'auto' }}>
          <Grid>
            {this.props.viewer.drivers.edges.map((driver) => {
              const CarImage = `url(${driver.node.img}) center / cover`;
              return (
                <Cell col={6} key={driver.node.id}>
                  <Card shadow={1} className="driverCard" style={{width: '100%'}}>
                    <CardTitle style={{ color: '#fff', height: '176px', background: `${CarImage}` }}>{driver.node.name}</CardTitle>
                    <CardText>Available: {driver.node.from} - {driver.node.to}</CardText>
                    <CardText>Car model: {driver.node.modelAuto}</CardText>
                    <CardActions border>
                      <Button ripple>Order {driver.node.name}</Button>
                    </CardActions>
                  </Card>
                </Cell>
              );
            })}
            <Cell col={12}>
              <h3>Did not find what you were looking for?</h3>
              <AddDriverComponent viewer={this.props.viewer}/>
            </Cell>
          </Grid>
        </div>
      </Page>
    );
  }
}
