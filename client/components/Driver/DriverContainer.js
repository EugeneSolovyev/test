import Relay from 'react-relay';
import Driver from './DriverComponent';

export default Relay.createContainer(Driver, {
	fragments: {
		viewer: () => Relay.QL`
            fragment on User {
                id,
	            drivers(first: 20) {
		            edges {
			            node {
				            id,
				            name,
				            modelAuto,
				            from,
				            to,
				            img
			            }
		            }
	            }
            }`
	}
});
