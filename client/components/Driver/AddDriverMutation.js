import Relay from 'react-relay';

class AddDriverMutation extends Relay.Mutation {
  getMutation() {
    return Relay.QL`
			mutation { addDriver }
		`;
  }

  getVariables() {
    return {
      name: this.props.Driver.name,
      modelAuto: this.props.Driver.modelAuto,
      from: this.props.Driver.from,
      to: this.props.Driver.to,
      img: this.props.Driver.img
    };
  }

  getFatQuery() {
    return Relay.QL`
		fragment on AddDriverPayload {
	        driverEdge,
			viewer { drivers }
		}
	`;
  }

  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'viewer',
      parentID: this.props.viewerId,
      connectionName: 'drivers',
      edgeName: 'driverEdge',
      rangeBehaviors: {
        '': 'append'
      },
    }];
  }
}

export default AddDriverMutation;
