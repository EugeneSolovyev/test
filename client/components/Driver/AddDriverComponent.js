import React from 'react';
import Relay from 'react-relay';
import FileBase64 from 'react-file-base64';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import AddDriverMutation from './AddDriverMutation';
import {
	Card,
	CardTitle,
	CardActions,
	CardText,
	Button,
	Textfield,
	Cell,
	Grid
} from 'react-mdl';

export default class AddDriverComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Driver: {
        name: '',
        modelAuto: '',
        from: '',
        to: '',
        img: '',
        imgPreview: ''
      }
    };
  }

  handleUpdateField = (name, field) => {
    const State = this.state.Driver;
    for (const stateField in State) {
      if (stateField == name) {
        State[stateField] = field.target.value;
      }
    }
    this.setState({
      Driver: State
    });
  };

  handleUpdateDate = (name, date) => {
    const State = this.state.Driver;
    const formattedDate = moment(date).format('DD.MM.YYYY');
    for (const dateField in State) {
      if (dateField == name) {
        State[dateField] = formattedDate;
      }
    }
    this.setState({ Driver: State });
  }

  getFiles = (files) => {
    this.setState(prevState => ({
      ...prevState.Driver,
      img: files.base64,
      imgPreview: files.base64
    }));
  }

  handleAddUser = (e) => {
    e.preventDefault();
    const addDriverMutation = new AddDriverMutation({
      viewerId: this.props.viewer.id,
      Driver: this.state
    });
    Relay.Store.commitUpdate(addDriverMutation);
  };

  render() {
    const { Driver } = this.state;
    return (
      <div>
        <Grid>
          <Card shadow={0} style={{ width: '50%' }}>
            <form>
              <Cell col={12}>
                <Textfield
                  value={this.state.Driver.name}
                  onChange={this.handleUpdateField.bind(this, 'name')}
                  label="Name"
                />
              </Cell>
              <Cell col={12}>
                <Textfield
                  value={this.state.Driver.modelAuto}
                  onChange={this.handleUpdateField.bind(this, 'modelAuto')}
                  label="Car model"
                />
              </Cell>
              <Cell col={5}>
                <CardText>From:</CardText>
                <DatePicker
                  value={this.state.Driver.from}
                  onChange={this.handleUpdateDate.bind(this, 'from')}
                  startDate={moment()}
                  dateFormat="DD.MM.YYYY"
                />
              </Cell>
              <Cell col={5}>
                <CardText>To:</CardText>
                <DatePicker
                  value={this.state.Driver.to}
                  onChange={this.handleUpdateDate.bind(this, 'to')}
                  startDate={moment()}
                  dateFormat="DD.MM.YYYY"
                />
              </Cell>
              <Cell col={12}>
                <FileBase64
                  multiple={false}
                  onDone={this.getFiles.bind(this)}
                />
              </Cell>
            </form>
            <CardActions border>
              <Button ripple onClick={this.handleAddUser.bind(this)}>Add</Button>
            </CardActions>
          </Card>
          <Card shadow={0}>
            <CardTitle style={{ background: `url(${this.state.Driver.imgPreview}) center / cover` }} />
          </Card>
        </Grid>
      </div>
    );
  }
}
