CREATE TABLE User
(
  id INTEGER PRIMARY KEY,
  name VARCHAR(255)
);

INSERT INTO User('name')
VALUES
('freiksenet'),
('fson'),
('Hallie'),
('Sophia'),
('Riya'),
('Kari'),
('Estrid'),
('Burwenna'),
('Emma'),
('Kaia'),
('Halldora'),
('Dorte');
